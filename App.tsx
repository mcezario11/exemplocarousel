import React, {useRef, useEffect, useState, useCallback} from 'react';
import Carousel from 'react-native-anchor-carousel';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Dimensions,
  TouchableOpacity,
  LogBox,
} from 'react-native';

declare const global: {HermesInternal: null | {}};

const App = () => {
  const carouselRef = useRef<any>(null);
  const scrollRef = useRef<ScrollView>(null);
  let index = 0;
  const width = Dimensions.get('window').width;
  const data = [
    {nome: 'Home'},
    {nome: 'Nossa História'},
    {nome: 'Projetos'},
    {nome: 'Saiba mais'},
  ];
  const carouselScroll = () => {
    if (index === data.length - 1) {
      index = 0;
    } else {
      index += 1;
    }

    carouselRef.current.scrollToIndex(index);
    scrollRef.current?.scrollTo({x: index * 300});
  };
  useEffect(() => {
    LogBox.ignoreAllLogs();
    const interval = setInterval(carouselScroll, 2000);
    return () => clearInterval(interval);
  }, []);
  const _render = (item: any, index: any) => {
    // {index: number, item: any}
    return (
      <TouchableOpacity>
        <View style={{width: 300, height: 200}}>
          <Image
            style={{width: 200, height: 200}}
            source={{
              uri: 'https://picsum.photos/200',
            }}></Image>
          <Text style={{color: 'red'}}>{item.item.nome}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <ScrollView>
      <View style={styles.carouselContainer}>
        <Carousel
          style={styles.carousel}
          data={data}
          renderItem={_render}
          itemWidth={200}
          containerWidth={width - 20}
          separatorWidth={0}
          ref={carouselRef}
          //pagingEnable={false}
          //minScrollDistance={20}
        />
      </View>
      <View style={styles.carouselContainer}>
        <ScrollView horizontal={true} ref={scrollRef}>
          {data.map((v, i) => _render({item: v}, i))}
        </ScrollView>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  carouselContainer: {
    height: 400,
  },
  carousel: {
    flex: 1,
  },
});

export default App;

// const f = useCallback(() => {
//   console.log(index);
//   if (index === data.length - 1) {
//     setIndex(0);
//     console.log('entrou 2');
//   } else {
//     setIndex(index + 1);
//     // console.log(index + 1);
//   }

//   carouselRef.current.scrollToIndex(index);
// }, [index]);
// useEffect(() => {
//   console.log('Chamou');
//   setInterval(() => {
//     f();
//     // console.log(index);
//   }, 2000);
// }, []);
